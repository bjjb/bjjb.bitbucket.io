#!/usr/bin/env sh
s=bitbucket
tmux has-session -t $s
if [ $? != 0 ]
then
  tmux new-session -d -s $s -n build
  tmux send-keys -t $s:build "node_modules/.bin/cake serve" C-m
  tmux new-window -t $s -n edit
  tmux send-keys -t $s:edit "vim ." C-m
fi
tmux attach-session -t $s
