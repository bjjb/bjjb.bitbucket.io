Go Project Hierarchy
====================

One of the tricky aspect of [Go][] (IMO) is that there's no absolutely
canonical recommendation for organising a project. So, let's try to learn by
example.

The best source for Go code is very probably the Go standard library. That's
easy to find - just run `go env`, and look for the value of `GOROOT`.

```
$ go env | grep GOROOT
GOROOT="/usr/local/Cellar/go/1.2.1/libexec"
```

I'm on a Mac, and installed Go using [homebrew][], see.


