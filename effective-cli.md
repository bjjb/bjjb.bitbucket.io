Effective Command-Line Usage
============================

> The COMMANDER stands for the virtues of wisdom, sincerity, benevolence,
> courage and strictness.
<cite>Sun Tzu</cite>

I spend almost all of my working life (and too much of my normal life) at the
UNIX command-line. There are worse places to spend one's life, but like any
home, you need to know your way around.

Here are some tips for getting around like a ninja. Or at least, less like a
dugong in a giant slipper.

The first thing to know well (and that means, read up about, and practive
with) is [readline][]. Because unless you're unusual, that's what's handling
your input at your terminal.

1: Stop relying on the arrow keys
---------------------------------

> The difficulty of tactical maneuvering consists in turning the devious into
> the direct, and misfortune into gain.

You type a command to scp 1 file with a long name to another file on another
computer.

<kbd>scp /my/hidden/file/ me@blah.com:a/location/elsewhere<kbd>.

You've just finished typing, and you realize that `blah.com` has SSH open on
port 20000, instead of the default. So you have to go back and insert the
`-P 20000` just after the `scp`.

<kbd>←←←←←←←←←←←←←←←←←←←←←←←←←←←←←←←←←←←←←←←←←←←←←←←←</kbd>?

No! Just go to the start of the line with <kbd>Ctrl+A</kbd>, and then to the
end of the next word (`scp`) with <kbd>Alt+F</kbd>. (F for forward). Type
<kbd> -p 20000</kbd>, hit return, and you're done.

...more to come.

[readline]: http://cnswww.cns.cwru.edu/php/chet/readline/rltop.html
